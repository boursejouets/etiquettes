// récupération des paramètres id d'appel
$.ulrParam = function (param) {
    var results = new RegExp('[\?&]' + param + '=([^&#]*)')
        .exec(window.location.search);

    return (results !== null) ? results[1] || 0 : '001';
}

function generateBarcode() {

    var idFiche = String($.ulrParam('id'));

    $("#titre").text("Codes Liste " + idFiche);

    $("#nomdoc").text("Bourse2019-" + idFiche);

    // Complilation du template
    var template = Handlebars.compile($("#tmpl").html());

    var max = 30;
    for (var i = 1; i <= max; i++) {

        var idProduit = String(i).padStart(2, '0');
        var code = idFiche + '-' + idProduit;
        var identifiant = 'c' + idFiche + idProduit;

        var codeBare = (idFiche + '0' + idProduit).padEnd(12, '0');

        // Création de la div
        var data = { identifiant: identifiant, code: code };

        $("#planche").append(template(data)).fadeIn();

        var btype = 'ean13';
        var settings = {
            output: 'bmp',
            bgColor: '#FFFFFF',
            color: '#000000',
            showHRI: false,
            barWidth: 2,
            barHeight: 25,
            moduleSize: 5,
            posX: 10,
            posY: 20,
            addQuietZone: 1
        };

        $("#" + identifiant).html("").show().barcode(codeBare, btype, settings);


    }

    // Impression du document
    // var doc = new jsPDF();
    // var specialElementHandlers = {
    //     '#ignorePDF': function (element, renderer) {
    //         return true;
    //     }
    // };

    // doc.fromHTML($('#planche').html(), 15, 15, {
    //     'width': 170,
    //     'elementHandlers': specialElementHandlers
    // });
    // //doc.save('sample-file.pdf');
    // doc.output('sample-file.pdf');

}
